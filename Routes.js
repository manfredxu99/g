import Homepage from './src/app/homepage'
import Contact from './src/app/contact'
import Work from './src/app/work'
import Portfolio from './src/app/portfolio'
import App from './src/app/app'
import { Route, IndexRoute } from 'react-router'
import React from 'react'

module.exports = (
<Route path='/' component={App}>
	<IndexRoute component={Homepage} />
	<Route path='home' component={Homepage} />
	<Route path='contact' ref="contactRef" component={Contact} />
	<Route path='work' component={Work} />
	<Route path='portfolio' component={Portfolio} />
</Route>
)