const gulp = require('gulp'),
      autoprefixer = require('autoprefixer'),
      webpack = require('webpack'),
      WebpackDevServer = require('webpack-dev-server'),
      gutil = require('gulp-util'),
      clean = require('gulp-clean')

const webpackConfig = require('./webpack.config.babel.js')

gulp.task('help', require('gulp-task-listing'))
// The development server (the recommended option for development)
gulp.task('default', ['webpack-dev-server'])

gulp.task('build-dev', ['webpack:build-dev'], () => {
  gulp.watch(['src/app/**/**'], ['webpack:build-dev'])
})

//Production Build
gulp.task('build-production',['webpack:build'])

gulp.task('webpack', /*may need dependencies */(err, stats) => {
  webpack(webpackConfig('web')).run((err, stats) => {
    gutil.log(stats.toString({errors: true, warnings: true, colors: true}))
  })

  webpack(webpackConfig('server')).run((err, stats) => {
    gutil.log(stats.toString({errors: true, warnings: true, colors: true}))
  })
})

gulp.task('clean', () => {
  return gulp.src(['./build/**/*', './public/**/*'])
  .pipe(clean())
})

gulp.task("webpack-dev-server", function(callback) {
  // modify some webpack config options
  var myConfig = Object.create(webpackConfig('server'))
  myConfig.devtool = "eval"
  myConfig.debug = true

  // Start a webpack-dev-server
  new WebpackDevServer(webpack(myConfig), {
    publicPath: myConfig.output.publicPath,
    stats: {
      colors: true
    }
  }).listen(8080, "localhost", function(err) {
    if(err) throw new gutil.PluginError("webpack-dev-server", err)
    gutil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server/index.html")
  })
});