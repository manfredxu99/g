import 'babel-polyfill'
import koa from 'koa'
import serve from 'koa-static'
import React from 'react'
import Promise from 'bluebird'
import views from 'co-views'
import App from './src/app/app'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import Homepage from './src/app/homepage'
import Portfolio from './src/app/portfolio'
import Work from './src/app/work'
import Contact from './src/app/contact'
import mysql from 'mysql'
import { Router as ReactRouter, Route, browserHistory, match, RouterContext } from 'react-router'
import Routes from './Routes.js'
import bodyParser from 'koa-bodyparser'
import _ from 'lodash'
import logger from 'koa-logger'

Promise.promisifyAll(require('mysql/lib/Connection').prototype)
Promise.promisifyAll(require('mysql/lib/Pool').prototype)

const app = koa(),
      router = require('koa-router')()

app.use(logger())
app.use(bodyParser())
app.use(router.routes())

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'contactform'
})

app.use(function* (next) {
	try {
		yield* next
	} catch (e) {
		console.log('blah', e)
	}
})

connection.connect((e) => {	
	if (e) {
		console.log('Error connecting: ' + e.stack)
		return
	}
	console.log('Connected as id ' + connection.threadId)
})

router.post('/handle', function* () {
	const results = this.request.body
	const post = {name: results.name, email: results.email, subject: results.subject, message: results.message}
	connection.query('INSERT INTO contacts SET ?', post, function(err, results) {
		if (err) alert('There were empty fields!')
		console.log(results)
	})
})

app.use(function* (next) {
  this.render = views('../views', {
    map: {html: 'ejs'},
    'default': 'ejs'
  })
  yield* next
})

app.use(serve('../public'))

app.use(function* (next) {
	let reactString
	match({routes: Routes, location: this.url}, (err, redirect, props) => {
		if (err) {
			this.throw(err.message)
		} else if (redirect) {
			this.redirect(redirect.pathname + redirect.search)
		} else if (props) {
			reactString = ReactDOMServer.renderToString(<RouterContext {...props} />)
		} else {
			this.throw('Not Found', 404)
		}
	})
	this.body = yield this.render('index', {
	renderedOutput: reactString
	})
	yield* next
})

app.listen(80, function* () {
	console.log('Koa Server running on port 80')
})

