'use strict'

import React from 'react'
import _ from 'lodash'
import { Router, Route, Link } from 'react-router'

class Navbar extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const tabs = _.map(this.props.navbarItems, (itemName) => {
          let key = itemName
          return (
            <li key={key}>
              <Link to={`/${itemName.toLowerCase()}`} activeClassName="active"> {itemName} </Link>
            </li>
          )
        })
    return (
      <nav id="nav">
        <ul>
          {tabs}
        </ul>
      </nav>
    )
  }
}

Navbar.defaultProps = {navbarItems: ['Home', 'Work', 'Portfolio', 'Contact']}

export default Navbar
