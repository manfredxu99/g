'use strict'

import './style.scss'
// import './style-mobile.scss'
import './style-desktop.scss'
import React from 'react'
import Navbar from './Navbar.js'
import { Link } from 'react-router'

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const page = this.props.page
    return (
      <div>
        <Navbar />
        {this.props.children}
      </div>
    )
  }
}

export default App
