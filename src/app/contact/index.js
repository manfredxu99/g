'use strict'

import React from 'react'
import { Link } from 'react-router'
import _ from 'lodash'
import request from 'superagent'
import classNames from 'classnames'

class Contact extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      subject: '',
      message: '',
      text: 'Send Message',
      submitted: false
    }
  }
  
  linkWithState = (key) => {
    return {
      value: this.state[key],
      requestChange: newValue => {
        const newState = this.state
        newState[key] = newValue
        this.setState(newState)
      }
    }
  };
  
submitForm = (e) => {
  for (let field in this.state) {
    if (/^(name|email|subject|message)$/.test(field)) {
      if (!this.state[field]) {
        alert('There were empty fields!')
        e.preventDefault()
        return
      } else if (!this.state[field].replace(/\s+/, '').length) {
        alert('There were empty fields!')
        e.preventDefault()
        return
      }
    }
  }
  e.preventDefault()
  this.setState({text: 'Message Sent!', name: '', email: '', subject: '', message: '', submitted: true})
  request
    .post('/handle')
    .send({name: this.state.name, email: this.state.email, subject: this.state.subject, message: this.state.message})
    .end(function(err, res) {
      if (err) {
        console.log(err)
      }
      console.log(res)
    })
  }

  
  clearForm = (e) => {
    this.setState({name: '', email: '', subject: '', message: ''})
  }

  render() {
    return (
      <div className="wrapper wrapper-style4">
        <article id="contact">
          <header>
            <h2>Want to get in touch?</h2>
            <span>Send me a message!</span> </header>
          <div className="5grid">
            <div className="row">
              <div className="12u">
                <form method="post" action="#">
                  <div className="5grid">
                    <div className="row">
                      <div className="6u">
                        <input type="text" name="name" id="name" placeholder="Name" valueLink={this.linkWithState('name')}></input>
                      </div>
                      <div className="6u">
                        <input type="text" name="email" id="email" placeholder="Email" valueLink={this.linkWithState('email')}></input>
                      </div>
                    </div>
                    <div className="row">
                      <div className="12u">
                        <input type="text" name="subject" id="subject" placeholder="Subject" valueLink={this.linkWithState('subject')}></input>
                      </div>
                    </div>
                    <div className="row">
                      <div className="12u">
                        <textarea name="message" id="message" placeholder="Message" valueLink={this.linkWithState('message')}></textarea>
                      </div>
                    </div>
                    <div className="row">
                      <div className="12u">
                        <input type="submit" className={classNames({submittedForm: this.state.submitted, button: true})} value={this.state.text} onClick={this.submitForm} disabled={this.props.submitted}></input>
                        <input type="reset" className="button button-alt" value="Clear Form" onClick={this.clearForm}></input>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="row row-special">
              <div className="12u">
                <h3>Links</h3>
                <ul className="social">
                  <li className="dribbble"><a href="/resume.pdf" target="_blank"><i className="fa fa-file-text-o icon fa-lg"></i></a></li>
                  <li className="linkedin"><a href="https://www.linkedin.com/in/manfredxu99" target="_blank"><i className="fa fa-linkedin icon fa-lg"></i></a></li>
                  <li className="tumblr"><a href="http://github.com/manfredxu99" target="_blank"><i className="fa fa-github icon fa-lg"></i></a></li>
                  <li className="googleplus"><a href="https://plus.google.com/101977603447613627442" target="_blank"><i className="fa fa-google-plus icon fa-lg"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
          <footer>
            <p id="copyright">© Manfred Xu 2016.</p>
          </footer>
        </article>
      </div>
    )
  }
}

export default Contact
