'use strict'

import React from 'react'
import { Link } from 'react-router'

class Homepage extends React.Component {
  render() {
    return (
      <div className="wrapper wrapper-style1 wrapper-first">
        <article className="5grid-layout" id="top">
          <div className="row">
            <div className="4u"> <span className="me image image-full"><img src={require('../images/me.jpg')} alt=""></img></span> </div>
            <div className="8u">
              <header>
                <h1>Hi there, I'm Manfred Xu!</h1>
              </header>
              <p>I'm currently seeking an entry level position as a front end engineer, looking to develop and apply my skills in order to provide amazing user experiences to delight and amaze your customers. <br/><br/>I also enjoy reading, music and playing ping pong.</p>
              <Link to={"/work"} className='button button-big'> Learn about what I do </Link> </div>
          </div>
        </article>
      </div>
    )
  }
}

export default Homepage
