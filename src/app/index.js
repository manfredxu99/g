import ReactDOM from 'react-dom'
import App from './app'
import Homepage from './homepage'
import Contact from './contact'
import Portfolio from './portfolio'
import Work from './work'
import { Router, Route, Link, browserHistory } from 'react-router'
import React from 'react'
import Routes from '../../Routes.js'
import './init'
const page = <Homepage />

const head = document.getElementsByTagName('head')[0]
const link = document.createElement('link')
link.href=require('./images/favicon.jpg')
link.rel="icon"
link.type="image/x-icon"
head.appendChild(link)

ReactDOM.render(
	<Router routes={Routes} history={browserHistory}/>,
	document.getElementById('output-container')
)


	// <App page={page} />, 