'use strict'

import React from 'react'
import { Link } from 'react-router'

class Portfolio extends React.Component {
  render() {
    return (
      <div className="wrapper wrapper-style3">
        <article id="portfolio">
          <header>
            <h2>Something</h2>
            {/*<span>Proin odio consequat sapien vestibulum ipsum primis sed amet consequat lorem dolore feugiat lorem ipsum dolore.</span>*/} </header>
          <div className="5grid-layout">
            <div className="row">
              <div className="12u"> </div>
            </div>
            <div className="row">
              <div className="4u">
                <article className="box box-style2"> <a href="#" className="image image-full"><img src={require('../images/placeholder.jpg')} alt=""></img></a>
                  {/*<h3><a href="#">Magna feugiat</a></h3>
                                    <p>Ornare nulla proin odio consequat.</p>*/}
                </article>
              </div>
            </div>
          </div>
          <footer>
            <p>Interested?</p>
            <Link to={'/contact'}className="button button-big">Get in touch with me</Link></footer>
        </article>
      </div>
    )
  }
}

export default Portfolio
