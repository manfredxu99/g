'use strict'

import React from 'react'
import { Link } from 'react-router'

class Work extends React.Component {
  render() {
    console.log(this.props)
    return (
      <div className="wrapper wrapper-style2">
        <article id="work">
          <header>
            <h2>I build cool software, primarily web apps.</h2>
            <span></span> </header>
          <div className="5grid-layout">
            <div className="row">
              <div className="4u">
                <section className="box box-style1"> <div className="image image-centered skills-centered">
                <img src={require('../images/html5.png')} alt=""></img>
                <img src={require('../images/css3.png')} alt=""></img>
                <img src={require('../images/node.png')} alt=""></img>
                <img src={require('../images/react.png')} alt=""></img>
                <img src={require('../images/jquery.png')} alt=""></img>
                <img src={require('../images/git.png')} alt=""></img>
                </div>
                  <h3>Skills</h3>
                  <p>HTML5, CSS3, Javascript/Node.js, React, jQuery, Git and more.</p>
                </section>
              </div>
            </div>
          </div>
          <footer>
            {/*<p>Lorem ipsum dolor sit sapien vestibulum ipsum primis?</p>*/}
            <Link to={'/portfolio'} className="button button-big">See some of my recent work</Link> </footer>
        </article>
      </div>
    )
  }
}

export default Work
