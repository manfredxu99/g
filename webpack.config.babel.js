'use strict'
const path = require('path'),
      autoprefixer = require('autoprefixer'),
      assetsPlugin = require('assets-webpack-plugin'),
      assetsPluginInstance = new assetsPlugin(),
      fs = require('fs'),
      ExtractTextPlugin = require('extract-text-webpack-plugin')

// const sassLoaders = ['css-loader', 'postcss-loader', 'sass-loader']
module.exports = target => {
  const config = {
    entry: {app: path.join(__dirname, 'src', 'app')},
    output: {
      path: path.join(__dirname, 'public'),
      filename: '[name].js'
    },
    module: {
      loaders: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015', 'stage-0']
          }
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader!sass-loader')
        },
        {
          test: /\.(png|jpg|jpeg|gif|woff|svg|ico)$/,
          loader: 'url-loader?limit=8192'
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000&minetype=application/font-woff"
        },
        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
        }
      ]
    },
    postcss: [ autoprefixer({ browsers: ['last 2 versions']}) ],
    plugins: [
      new ExtractTextPlugin('[name].css', {allChunks: true})
    ],
    resolve: {
      extensions: ['', '.js', '.sass'],
      modulesDirectories: ['node_modules', 'src']
    },
    node: {
      __dirname: true
    }
  }

  const nodeModules = {};
  fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

  switch (target) {
  case 'server':
    config.entry = {app: path.join(__dirname, 'index.js')}
    config.output = {
      path: path.join(__dirname, 'build'),
      filename: '[name].js',
      libraryTarget: 'commonjs2'
    };
    config.target = 'node';
    config.externals = nodeModules;
    config.node = {
      __filename: true
    }
    break;
  case 'web':
    config.devtool = 'source-map';
    if (process.env.NODE_ENV === 'production') {
      config.plugins = [assetsPluginInstance, new webpack.optimize.UglifyJsPlugin({minimize: true}), new ExtractTextPlugin('[name].css', {allChunks: true})]
    } else {
      config.plugins = [assetsPluginInstance, new ExtractTextPlugin('[name].css', {allChunks: true})];
    }
    break;
  }
  return config
}
